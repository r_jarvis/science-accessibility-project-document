    ---
- title: 'The Science Accssibility Project'
tags:
  - example
  - tags
  - for the paper
authors:
 - name: Russell Jarvis
   orcid: 0000-0003-0281-2849
   affiliation: "1, 2"
 - name: Patrick McGurrin
   orcid: 0000-0003-0872-7098
   affiliation: 2
affiliations:
 - name: ASU school of Life Sciences.
   index: 1
 - name: .
   index: 2
date: 14 February 2016
bibliography: paper.bib
---

## Abstract
In order to better characterize the accessibility of scientific and non scientific writings, we describe the process of machine classifying a wide variety of written word from the internet,  we show that between document types, writing styles are predictable enough that a machine can classify, whom the intended audience of a paper is. We show that science writing, is predictably complicated, and non science writing is predictably engaging and simple.


### Keywords
Bag of words analysis, Cognitive Load, Writing Complexity, naive Bayes.\\

## Introduction



Non-science exceeds genuine scientific writing in one important criteria: in contrast to genuine science, non-science ideas are expressed with a more engaging and lower complexity writing style.  

A broader range of stakeholders conceivably benefit from the  expression of ideas with language that is of lower complexity. This ensures that words and ideas are  more readily mobilized and can be transferred into public awareness. Related, machine readability and machine organization of factual information derived from journal articles should also occur more readily. 

This technique of writing using accessible language is one we see often in magazines, newspapers, and other online content, as the writing complexity level of general readership is about an 8th grade level (source and fact check). 

Alternatively, we find that there is a trend in academic writing to use complex language to explore and document ideas. This likely stems from the goal of publishing in scientific journals, to which scientists are documenting experiments and ideas for other scientists. However, we believe that this writing style affects how scientists communicate their work in other media, including blogs, news, and other similar media outlets. 

The tendency for scientists to communicate in such a complex way, irrespective of media outlet, harms their ability to spread useful, factual information to the general public. Because scientists write at such a complex level, the most readable and findable online information for the general public related to science may then come from other groups, and may then be potentially less accurate. One might also note this may be especially true regarding controversial issues. This difficulty of finding and understanding accurate scientific information may then contribute to a general misunderstanding of scientific fact. 

## Key Findings

* Wikipedia writing, is highly predictable in style. The predictablity in writing style makes classification against style possible. Wikipedia articles do well mimic traditional scientific writing.
* the Wikipedia corpus constitutes a controlled natural language. With low variance in writing complexity, and sentiment between articles.
\item The Wikipedia writing often fulfils, important aspects of the scientific method, without always being experimental in nature. 
\item Since wikipedia pages are artificially elevated to page rank0, the impact of promoting this fact based website, could be further increased if writing style was optimized for improved accessibility.
\item Readability does not decrease with page rank unless, Wikipedia articles are controlled for.
% Wikipedia occurs in the public, is subject to public scrutiny, and it is open access.
\end{itemize}


## Methods

We built a web-scraping and written text analysis infrastructure by extending many existing Free and Open Source (FOS) tools. The Web scrapping interface employs several common python modules, chief among those was: Textstat, Google Scrape, Beautiful Soup and Selenium. The Text analysis infrastructure was based on the two substantial code bases Textstat, which contained measures of text reading level (complexity), and NLTK (the Natural Language Processing Tool Kit), which contained measures of text subjectivity, and sentiment type.

The scraping, and analysis work we performed, rested on top of a large hierarchy of software dependencies. However, it is increasingly well understood, that, dependency heavy software stacks act as a significant impediment to investigating or reproducing any product of digital, scholary research. In order to address this problem and to enhance the reproducibility of our approach, we created the necessary web-scrapping, and analysis infrastructure inside a dedicated Docker Container.

Reproducibility is burdened by the technical task of satisfying each software dependencies necessary to recreate a digital scientific experiment. Our position, of starting with from a cloned software environment, will mitigate, the burden of duplicating our digital research environment. That is why we have used a Docker file, and associated Docker container together, as they act as self-documenting and extremely portable software environment clone.

Initially we created two different, unrelated and broad ranging lists of scientific queries. The first type of query was predominantly cultural in nature, or world view related. The second set of queries represented gains in knowledge about physical entities or physical processes in the world. We were interested in scientific, and pseudo-scientific writing.\\

There were two types of writing that we actively excluded from our analysis, those were websites expressed in a non-English language, and also websites, that were highly commercial in nature. These were websites advertisements, of consumer goods, and online shopping generally. These websites, utilizing wording, that significantly biases text stat metrics. Webpages of less than 400 words, were most often advertisements, and websites of a comercial nature such as Amazon shopping.\\

Non English, websites were excluded for the simple reason that they are not amenable to Textstat, and NLTK tools, however, even if this was not the case, it is also known, that there are  significant differences in per-word information entropy between different natural languages.

### Search Engine Queries:
The first two lists of queries were chosen to be belong to an overt set of exclusively scientific or cultural search terms. A third list of terms was designed to be deliberately ambiguous.

### Science Queries:
The three lists of search engine queries were as implemented as follows: science engine queries: 'evolution', 'photo-sysnthesis' ,'Transgenic', 'GMO', 'climate change', 'cancer', 'Vaccines', 'Genetically Modified Organism', �differential equation�

### Cultural queries were as follows:} 'reality TV', 'prancercise philosophy', 'play dough delicious deserts', 'unicorn versus brumby', 'football soccer' , god fearing.

As discussed we also designed ambiguous queries which were equally likely return content that was either scientific, or non scientific in nature. The reason for doing this was to provide a challenge for the classification algorithm.

### Ambiguous Queries, were as follows:

"the singularity","skynet","","killer robot","franken-science","Frankenstein,�the God Delusion","god does not play dice", "the selfish gene","political science", ", "requim for a spike",'psycho-physics','soma'.
The list of ambiguouss queries was formulated in order to better challenge the robustness of the machine classification algorithm.

After scraping across the two different lists were performed, the resulting queries were filtered, according to specific sets of criteria. As stated previously, we discarded from our analysis, web pages that were not written in English, since we did not have the necessary tools, to analyze them.

### Text Metrics:
A list of metrics applied to downloaded corpus include: TextStat, which was used to measure word complexity (an average of several important word complexity metrics, such as the Gunning Fog measure of reading level), LZW compression-ratio, de-compression ratio, sentiment analysis, subjectivity analysis, and page rank.

Compression ratios were used to investigate the notion, that well written scientific writing, might simply be lower in information entropy, and an information theoretic analysis, can be used to both to better characterize, and corroborate the findings of other reading word complexity metrics

## Reference Texts:

Some reference texts were used, as a means of providing contrast, and context, to data points, among our web scraping derived corpus. The Upgoer5 is a library, of scientific texts, written with the aid of a text editor, which imposes, that output documents are exclusively comprised by, only the 10,000 most commonly occurring English words.

The Post Modern Essay Generator (PMEG), embodies an artificial English synthesis technique. Documents that are output from the website, consist of sentences that obey the rules of written English, however there are no restraints governing the semantic conceptual references in the sentences. If any particular sentence in a PMEG document, embodies an objective meaning, it is only by chance. Output from PMEG reads as highly coded, and vague.

The reference data points in some ways provide further validation to the existing text metric tools, as we needed to verify that word readability metrics provided results that were consistent with prior assumptions about known texts. For instance, the corpus derived from upgoer editor, should require a very low reading grade level to understand. Texts, from the PMEG should require a very high reading level to understand, and cumulative entropy of such texts should be high.



\begin{center}
    \begin{tabular}{ | l | p{5cm} |}
    \hline
    complexity & texts  \\ \hline
    6.0 & upgoer5 \\
    9.0 & readability of science declining over time\\
    14.0 & science of writing \\ 
    12.0 & post modern essay generator \\
    14.9 & mean wikipedia article \\
     \hline
    \end{tabular}
\end{center}

\verb|\documentclass[bookreview,manuscript]{clv3}|

%\subsection{Default Option}

\begin{deflist}
\item[bookreview] Sets the article layout for Book Review.
\item[brief] Sets the article layout for Briefly Noted.
\item[discussion] 
\item[pubrec] Sets the article layout for Publication Received.
\item[manuscript] Sets the baseline spacing to double space. This
option can be used in combination with other options.
\end{deflist}

By not declaring any option in the \verb|\documentclass| command the class file
will automatically set to standard article layout.

## Results

We created a total list all of the different queries obtained, from combining both the list of cultural queries, and scientific queries, and then applied such queries exclusively to the Wikipedia, search interface. We take the result of evaluating this pool of queries, and then plot the resulting pool of queries versus page rank. The Wikipedia, actually showed a small but consistant preference for web pages of higher complexity.

The plots below may appear to look a bit unprofessional, as not all data points have error bars. The reason for this, is pandas+seaborne allows you to plot on the same axis multiple sample data points, and single sampled data points. Only of the five mentioned search engines was made to sample beyond page rank 10, but all five sampled under page rank 10.

The same plot but with mean and std deviation plots when multiple samples per page rank are available. When we plotted the Wikipedia queries, where reading (text-stat standard) level is instead plotted against page rank, we again see that there is a slight trend towards increasing text complexity with decreasing page rank. 


If instead we consider how resistant Wikipedia pages are to compression, we see that low page rank pages, are more resistant to compression. This finding recapitulates the same result as the above figure, where increasing Wikipedia page rank slightly decreases text complexity.

When aggregated search results between all possible search engines, and then plotting page rank versus complexity, for particular types of search terms, there was a strong positive correlation between page rank, and reading level.

In the case of GMO, and Genetically Modified Organism, positive increasing trends where observed

The title page is created using the standard \LaTeX\ command \verb|\maketitle|.
Before this command is declared, the author must declare all the data which are
to appear in the title area.\footnote{$\backslash$maketitle is the command to execute all the title page information.}

## Discussion

### The issue of Irreducible Complexity
It has been observed that the readability of scientific texts is declining with increased time[]. Decreasing readability, is usually due to increased writting complexity. One could argue that scientific writing is increasing in complexity, as this reflects, the obvious advances in science methods and analysis, which increase the complexity in the act of simply doing science. Afterall manys cientific advances add details to existing complex models and imaging techniques.\\
\\
Scientific papers occur in lots of fields from particle physics to marine mammal acoustics. Concepts that are described in these fields may be attributed with different degrees of 'irreducible complexity'. Ie concepts that need to be communicated, are just unequally complicated, so even if two different papers were written in their theoretical best, simple and clear language, one paper would still be more complex than the other, just because one papers core concept was characterized by higher intrinsic irreducible complexity in its underlying conceptual framework.\\
\\
Given that the act of science is becoming more complex, is it reasonable to argue that all scientific writing needs to be complex in order to capture irreducible complexity? We argue that there are some great counter examples to the idea of irreducible complexity.\\
\\
The Stanford text stat algorithms employed here, is blind to differences in intrinsic irreducible complexity. I thought scaling the metrics by how dense they are with information entropy, could be a good correction factor, just like in the rap song approach. My thinking has now changed, to, this is not really something that can be corrected for, but it's just another issue to be aware of, and it would be great if any writing about the analysis showed awareness of this problem.




-![Fidgit deposited in figshare.](figshare_article.png)

# References

Kincaid, J. Peter, et al. "Derivation of new readability formulas (automated readability index, fog count and flesch reading ease formula) for navy enlisted personnel." (1975).
Kuhn, Tobias. (2016). The Controlled Natural Language of Randall Munroe's Thing Explainer. 
A. C. Bulhak, On the simulation of postmodernism and mental debility using recursive transition networks, Monash Univ. Dep. Comput. Sci., 1996.
V. Clayton, The Needless Complexity of Academic Writing, The Atlantic, 26-Oct-2015.


## Discussion

### The issue of Irreducible Complexity
It has been observed that the readability of scientific texts is declining with increased time[]. Decreasing readability, is usually due to increased writting complexity. One could argue that scientific writing is increasing in complexity, as this reflects, the obvious advances in science methods and analysis, which increase the complexity in the act of simply doing science. Afterall manys cientific advances add details to existing complex models and imaging techniques.\\
\\
Scientific papers occur in lots of fields from particle physics to marine mammal acoustics. Concepts that are described in these fields may be attributed with different degrees of 'irreducible complexity'. Ie concepts that need to be communicated, are just unequally complicated, so even if two different papers were written in their theoretical best, simple and clear language, one paper would still be more complex than the other, just because one papers core concept was characterized by higher intrinsic irreducible complexity in its underlying conceptual framework.\\
\\
Given that the act of science is becoming more complex, is it reasonable to argue that all scientific writing needs to be complex in order to capture irreducible complexity? We argue that there are some great counter examples to the idea of irreducible complexity.\\
\\
The Stanford text stat algorithms employed here, is blind to differences in intrinsic irreducible complexity. I thought scaling the metrics by how dense they are with information entropy, could be a good correction factor, just like in the rap song approach. My thinking has now changed, to, this is not really something that can be corrected for, but it's just another issue to be aware of, and it would be great if any writing about the analysis showed awareness of this problem.


[1] Kincaid, J. Peter, et al. "Derivation of new readability formulas (automated readability index, fog count and flesch reading ease formula) for navy enlisted personnel." (1975).
[2] Kuhn, Tobias. (2016). The Controlled Natural Language of Randall Munroe's Thing Explainer. 
[3]	A. C. Bulhak, On the simulation of postmodernism and mental debility using recursive transition networks, Monash Univ. Dep. Comput. Sci., 1996.
[4]	V. Clayton, The Needless Complexity of Academic Writing, The Atlantic, 26-Oct-2015.



